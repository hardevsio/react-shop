'use strict';

class Product {
	
	constructor ( Id, Name, Desc, Price, Manuf ) {

		this._id = Id;
		this._name = Name;
		this._description = Desc;
		this._price = Price;
		this._manufacture = Manuf;
	}

	set id( Id ) {
		this._id = Id;
	}

	get id() {
		return this._id;
	}

	set name( Name ) {
		this._name = Name;
	}

	get name() {
		return this._name;
	}

	set description( Desc ) {
		this._description = Desc;
	}

	get description() {
		return this._description;
	}

	set price( Price ) {
		this._price	= Price;
	}

	get price() {
		return this._price;
	}

	set manufacture ( Manuf ) {
		this._manufacture = Manuf;
	}

	get manufacture () {
		return $this._manufacture;
	}

	print() {
		console.log( "Id:", this._id );
		console.log( "Name:", this._name );
		console.log( "Description:", this._description );
		console.log( "Manufacturer:", this._manufacture );
		console.log( "Price:", this._price );
	}
}