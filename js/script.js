'use strict';

let products = [
	new Product( 1, 'Молоко', 'КМИ', 12.39,  'Ромол' ),
	new Product( 2, 'Сметана', '', 25.45,  'Ромол' ),
	new Product( 3, 'Хлеб', 'Салтовский', 10.50,  'СХЗ' ),
	new Product( 4, 'Огурец', 'Пупырышный', 25.0,  'Земля' ),
	new Product( 5, 'Помидор', 'Фиолетовый', 25.0,  'Земля' ),
	new Product( 6, 'Клубника', 'Гигантина', 25.0,  'Земля' ),
	new Product( 7, 'Яблоки', 'Райские', 25.0,  'Земля' ),
	new Product( 8, 'Помидор2', 'Фиолетовый', 25.0,  'Земля' ),
	new Product( 9, 'Помидор3', 'Фиолетовый', 25.0,  'Земля' ),
	new Product( 10, 'Помидор4', 'Фиолетовый', 25.0,  'Земля' ),
	
];


localStorage.products = JSON.stringify(products);


let pr2 = JSON.parse(localStorage.products, (key, value)=>{

	if(typeof(value) == "object" && !(value instanceof Array)){
		return new Product(value._id, value._name, 
			value._description, value._price, value._manufacture);
	}
	return value;
});


console.log(pr2);

window.DB = {

	products :pr2
}
