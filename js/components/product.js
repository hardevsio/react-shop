class ProductComponent extends React.Component {
    render() {
        return (
          
            <div className='col-md-3'>
                <div className="title">
                  <a href={"#"}>
                        {this.props.title}
                    </a>
                </div>
                
                <div className='description'>                 
                    <p>{this.props.description}</p>
                </div> 
            </div>       
        );
      }
}